package esfingeplugin.util;

import junit.framework.Assert;

import org.junit.Test;

public class ParserHelperTest {

	@Test
	public void testGetSimpleName() {
		String simpleName = ParserHelper.getSimpleName("org.esfinge.querybuilder.QueryObject");
		
		Assert.assertEquals("QueryObject",simpleName);
	}

}
